<?php


$data =
array(
    [index] => CLIPPINGS
    [type] => CLIPPING
    [thumbnail] => http://www.deloittetaxks.com.br/abcd/digital/EstudosDTT/capa//lei-anticorrupcao.jpg
    [987] => CL20170530141445
    [777] => 20140102
    [901A] => todos
    [digital] => http://www.deloittetaxks.com.br/abcd/digital/EstudosDTT/lei-anticorrupcao.pdf
    [16] => CLIPPING
    [245A] => Lei anticorrupção
    [245B] => um retrato das práticas de compliance na era de empresa limpa
    [591] => O Brasil está entrando em uma nova era de maturidade de seu ambiente de negócios. Com a entrada em vigor da Lei n. 12.846, também conhecida como “Lei Anticorrupção”, o País se alinha às mais rigorosas e avançadas legislações do mundo de combate à corrupção.; Este cenário traz um desafio para as organizações que atuam no Brasil, em termos da criação de uma estrutura de governança corporativa, gestão de riscos e controles internos. São mudanças profundas e que envolvem diretamente a cultura organizacional.; Com o objetivo de conhecer o estágio e as práticas das organizações neste sentido, a Deloitte apresenta o estudo “Lei Anticorrupção – Um retrato das práticas de compliance na era da empresa limpa”.
    [697] => Lei n. 12.846; Transparência; Riscos; Responsabilidade da empresa; Governança corporativa; Programa de conformidade
    [856B] => lei-anticorrupcao
    [903] => estudo
    [973] => ARTIGO
    [991] => Lei anticorrupção; um retrato das práticas de compliance na era de empresa limpa
    [995] => 2014
    [996] => 01
    [997] => 02
    [999] => <strong>Lei anticorrupção: um retrato das práticas de compliance na era de empresa limpa. </strong>02.01.2014
);
